interface ApiConstantsInterface {
  baseUrl: string;
  productEndpoint: string;
  headers: any;
}

const apiConstants: ApiConstantsInterface = {
  baseUrl: "https://fakestoreapi.com",
  productEndpoint: `/products`,
  headers: {'Access-Control-Allow-Origin': '*'}
};

export { apiConstants, ApiConstantsInterface };
