import { Product } from "@/models/product";
import Axios, { AxiosInstance } from "axios";
import {
  apiConstants,
  ApiConstantsInterface,
} from "../constants/api.constants";

interface IFakeStoreService {
  getProducts(): Promise<Product[]>;
  createProduct(): Promise<Product>;
  deleteProduct(): Promise<Product>;
  //getProduct(id: number): Promise<Product | null>;
  //updateProduct(): Promise<Product>;
}

// The docs for consuming this API can be found here: https://fakestoreapi.com/docs
class FakeStoreService implements IFakeStoreService {
  protected axios: AxiosInstance;

  protected config: ApiConstantsInterface;

  constructor(config: ApiConstantsInterface) {
    this.config = config;
    this.axios = Axios.create({
      baseURL: config.baseUrl,
      headers: config.headers
    });
  }

  // Docs: https://fakestoreapi.com/docs#p-all
  async getProducts(): Promise<Product[]> {
    const response = await this.axios.get(this.config.productEndpoint);
    return response.data;
  }

  // Docs: https://fakestoreapi.com/docs#p-new
  async createProduct(): Promise<Product> {
    // TODO: Implement this method
    throw new Error("Method not implemented");
  }

  // Docs: https://fakestoreapi.com/docs#p-delete
  async deleteProduct(): Promise<Product> {
    // TODO: Implement this method
    throw new Error("Method not implemented");
  }

  // Docs: https://fakestoreapi.com/docs#p-update
  // async updateProduct(): Promise<Product> {}

  // Docs: https://fakestoreapi.com/docs#p-single
  // async getProduct(id: number): Promise<Product | null> {}
  
}

const fakeStoreService = new FakeStoreService(apiConstants);
export { fakeStoreService };
